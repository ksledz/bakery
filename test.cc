#include <iostream>
#include "pie.h"
//#include "cake.h"
#include "bakery.h"
using namespace std;

int main() {
  ApplePie<int, 5, double> p(20, 100);
  cout << p.getStock() << endl;
  CherryPie<int, 1> d(100);
  Bakery<double, int, 100> b();
  //b.sell<CherryPie<int, 1>>();
//  cout << b.getProductStock<CherryPie<int, 1>>()<<endl;
//  b.restock<ApplePie<int, 5, double>>(2);
//  cout << b.getProductStock<ApplePie<int, 5, double>>()<<endl;
  /* simple unique types test */
  //TODO assertion of price types
  ApplePie<int, 3, float> q(28, 100);
  ApplePie<int, 5, float> r(22, 370);
  // this compiles and it should not compile
  Bakery<float, int, 200, ApplePie<int, 5, float>, ApplePie<int, 3, float>> b1(r, q);
  // this fails the assertion as it should
  //   Bakery<double, int, 100, ApplePie<int, 3, float>, ApplePie<int, 3, float>> b2(q, r);

}
