#ifndef CAKE_H
#define CAKE_H

#include <cmath>

namespace cake_utils {

  constexpr int sumlimit = 25;

  constexpr double element(int n) {
    return 1 / (n * pow(2, n));
  }

  constexpr double seriessum(int l) {
    return l >= 1 ? element(l) + seriessum(l - 1) : 0;
  }

  constexpr double ln2 = seriessum(sumlimit);
  struct emptyCakePrice {
  };
}


/**
 * Cake template
 * @tparam T type of length/width, must be integral
 * @tparam length length of a cake
 * @tparam width width of a cake
 * @tparam sellable whether a cake can be sold
 * @tparam P price type, has to be floating point
 */
template<typename T, T length, T width, bool sellable, typename P>
class Cake {

  static_assert(std::is_integral<T>::value,
                "Length type should be integral");
  static_assert(!sellable || std::is_floating_point<P>::value,
                "Price type should be floating point");

private:

  int stock;
  P price;

public:

  using price_type = P;
  using area_type = T;

  /**
   * Cake constructor for cakes that can be sold
   * @tparam hasPrice must be true to enable the constructor
   * @param initialStock initial stock of cake
   * @param price price of a cake
   */
  template<bool hasPrice = sellable>
  Cake(typename std::enable_if<hasPrice, int>::type initialStock, P price) :
    stock(initialStock),
    price(price) {};

  /**
   * cake constructor for cakes that cannot be sold
   * @tparam hasPrice must be false to enable the constructor
   * @param initialStock stock of cakes
   */
  template<bool hasPrice = sellable>
  explicit Cake(typename std::enable_if<!hasPrice, int>::type initialStock) :
    stock(initialStock) {};

  /**
   * @return stock of cakes
   */
  int getStock() const {
    return stock;
  }

  /**
   * @return area of one cake, explicitly casted from
   * whatever integral type length and width is onto double
   */
  static constexpr double getArea() {
    return cake_utils::ln2 * (double) length * (double) width;
  }

  /**
   * Sells one cake, subtracting it from stock
   * @tparam canSell must be true to enable this method
   */
  template<bool canSell = sellable>
  typename std::enable_if<canSell, void>::type sell() {
    if (stock > 0) --stock;
  };

  /**
   * @tparam canSell must be true to enable this method
   * @return price of a cake
   */
  template<bool canSell = sellable>
  typename std::enable_if<canSell, P>::type getPrice() const {
    return price;
  };


};

template<typename T, T length, T width> using CheeseCake = Cake<T, length, width, false, cake_utils::emptyCakePrice>;
template<typename T, T length, T width, typename P> using CreamCake = Cake<T, length, width, true, P>;

#endif
