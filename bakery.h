#ifndef BAKERY_H
#define BAKERY_H

#include <type_traits>
#include <tuple>

namespace bakery_utils {

  /**
   * Checks if type Tp is contained in List.
   * @tparam Tp
   * @tparam List
   */
  template<typename Tp, typename... List>
  struct contains {
    static constexpr bool value = true;
  };
  template<typename Tp, typename Head, typename... Rest>
  struct contains<Tp, Head, Rest...> {
    static constexpr bool value = std::is_same<Tp, Head>::value
                                  ? true
                                  : contains<Tp, Rest...> ::value;
  };
  template<typename Tp>
  struct contains<Tp> {
    static constexpr bool value = false;
  };


  /**
   * Checks whether Types have non-zero size.
   * @tparam Type
   */
  template<typename ...Types>
  struct is_non_empty {
    static constexpr bool value = false;
  };
  template<typename T, typename ...Types>
  struct is_non_empty<T, Types...> {
    static constexpr bool value = true;
  };


  /**
   * Checks whether C is a sellable product.
   * @tparam C
   */
  template<typename C, typename = void>
  struct is_sellable {
    static const bool value = false;
  };
  template<typename C>
  struct is_sellable<C, typename std::enable_if<
    std::is_same<decltype(std::declval<C>().sell()), void>::value>::type> {
    static const bool value = true;
  };


  /**
   * Checks whether C is a restockable product.
   * @tparam C
   */
  template<typename C, typename = void>
  struct is_restockable {
    static constexpr bool value = false;
  };
  template<typename C>
  struct is_restockable<C, typename std::enable_if<
    std::is_same<decltype(std::declval<C>().restock(0)), void>::value>::type> {
    static constexpr bool value = true;
  };


  /**
   * Checks if Types are all unique.
   * @tparam Types
   */
  template<typename... Types>
  struct are_all_unique {
    static constexpr bool value = true;
  };
  template<typename T, typename... Rest>
  struct are_all_unique<T, Rest...> {
    static constexpr bool value = (!std::is_same_v<T, Rest> && ...)
                                  && are_all_unique<Rest...>::value;
  };


  /**
   * Checks if sellable products among Prods
   * have price-type equal T.
   * @tparam T
   * @tparam Prods
   */
  template<typename T, typename... Prods>
  struct validate_price_types {
    static constexpr bool value = ((std::is_same_v<T, typename Prods::price_type>
                                    || !is_sellable<Prods>::value) && ...);
  };


  /**
   * Computes sum of areas of all product types.
   * @tparam Prods
   */
  template<typename ...Prods>
  struct areasum {
    static constexpr double value = (Prods::getArea() + ...);
  };


  /**
   * Checks if Prods have area-type equal A.
   * @tparam A
   * @tparam Prods
   */
  template<typename A, typename ...Prods>
  struct valid_area_types {
    static constexpr bool value = (
      std::is_same<A, typename Prods::area_type>::value && ...);
  };
}


template<class C, class A, A shelfArea, class ...P>
class Bakery {

private:

  static_assert(std::is_floating_point<C>::value,
                "Price-type must be a floating_point");
  static_assert(std::is_integral<A>::value,
                "ShelfArea-type must be integral");
  static_assert(bakery_utils::is_non_empty<P...>::value,
                "Bakery needs to have some products");
  static_assert(bakery_utils::are_all_unique<P...>::value,
                "All product types must be unique");
  static_assert(bakery_utils::validate_price_types<C, P...>::value,
                "Price-types in Products non-compatible with price-type of Bakery");
  static_assert(bakery_utils::valid_area_types<A, P...>::value,
                "Area-types in Products non-compatible with area-type of Bakery");
  static_assert((double) shelfArea >= bakery_utils::areasum<P...>::value,
                "Shelves can't hold all these baked goods");

  std::tuple<P...> products;
  C profits;

public:

  /**
   * Constructor for Bakery.
   * @param products
   */
  explicit Bakery(P... products) :
    products(products...),
    profits(0) {}

  /**
   * Public getter for profits.
   * @return
   */
  C getProfits() const {
    return profits;
  }

  /**
   * Sells sellable products,
   * it decreses product's stock,
   * while increasing bakery's profits.
   * @tparam Product
   */
  template<class Product>
  void sell() {
    static_assert(bakery_utils::contains<Product, P...>::value,
                  "Given product is not a member of bakery.");
    static_assert(bakery_utils::is_sellable<Product>::value,
                  "Tried to sell non-sellable product");

    Product &product = std::get<Product>(products);
    if (product.getStock() > 0) {
      profits += product.getPrice();
      product.sell();
    }
  };

  /**
   * Restocks restockable products.
   * @tparam Product
   * @param additionalStock
   */
  template<class Product>
  void restock(int additionalStock) {
    static_assert(bakery_utils::contains<Product, P...>::value,
                  "Given product is not a member of bakery.");
    static_assert(bakery_utils::is_restockable<Product>::value,
                  "Tried to restock non-restockable product");

    std::get<Product>(products).restock(additionalStock);
  };

  /**
   * Public getter for products stock.
   * @tparam Product
   * @return
   */
  template<class Product>
  int getProductStock() const {
    static_assert(bakery_utils::contains<Product, P...>::value,
                  "Given product is not a member of bakery.");

    return std::get<Product>(products).getStock();
  };
};

#endif
