#ifndef PIE_H
#define PIE_H

#include <cmath>

namespace pie_utils {

    constexpr int sumlimit = 1;
    constexpr double ramaconst = sqrt(8) / 9801;

    constexpr int factorial(int n) {
      return n > 0 ? n * factorial(n - 1) : 1;
    }

    constexpr double element(int k) {
      return (factorial(4 * k) * (1103 + 26390 * k)) /
             (pow(factorial(k), 4) * pow(396, 4 * k));
    }

    constexpr double seriessum(int l) {
      return l >= 0 ? element(l) + seriessum(l - 1) : 0;
    }

    /**
     * constexpr calculates pi approximation using Ramanujan's series
     */
    constexpr double pi = 1 / (ramaconst * seriessum(sumlimit));

    struct EmptyPiePrice {
    };
}
/**
 * Pie template
 * @tparam R type of radius, must be integral
 * @tparam radius radius of a pie
 * @tparam restockable whether pies can be restocked
 * @tparam sellable whether pies can be sold
 * @tparam P price type, must be floating point
 */

template<typename R, R radius, bool restockable, bool sellable, typename P>
class Pie {

    static_assert(std::is_integral<R>::value,
                  "Radius type should be integral");
    static_assert(!sellable || std::is_floating_point<P>::value,
                  "Price type should be floating point");

private:

    int stock;
    P price;

public:

    using price_type = P;
    using area_type = R;

    /**
     * Pie constructor for pies that can be sold
     * @tparam hasPrice must be true to enable the constructor
     * @param initialStock initial stock of pies
     * @param price price of a pie
     */
    template<bool hasPrice = sellable>
    Pie(typename std::enable_if<hasPrice, int>::type initialStock, P price) :
            stock(initialStock),
            price(price) {};

    /**
     * Pie constructor for pies that cannot be sold
     * @tparam hasPrice must be false to enable the constructor
     * @param initialStock stock of pies
     */
    template<bool hasPrice = sellable>
    explicit Pie(typename std::enable_if<!hasPrice, int>::type initialStock) :
            stock(initialStock) {};

    /**
     * @return stock of pies
     */
    int getStock() const {
      return stock;
    }
    
    /**
     * @return area of one pie, explicitly casted from
     * whatever integral type radius is onto double
     */
    static constexpr double getArea() {
      return pie_utils::pi * (double) radius * (double) radius;
    }

    /**
     * Sells one pie, subtracting it from stock
     * @tparam canSell must be true to enable this method
     */
    template<bool canSell = sellable>
    typename std::enable_if_t<canSell, void> sell() {
      if (stock > 0) --stock;
    };

    /**
     * @tparam canSell must be true to enable this method
     * @return price of a pie
     */
    template<bool canSell = sellable>
    typename std::enable_if<canSell, P>::type getPrice() const {
      return price;
    };

    /**
     * Method that restocks pies
     * @tparam canRestock must be true to enable this method
     * @param additionalStock how many pies are added to the stock
     */
    template<bool canRestock = restockable>
    typename std::enable_if<canRestock, void>::type restock(int additionalStock) {
      stock += additionalStock;
    };

};

template<typename R, R radius> using CherryPie = Pie<R, radius, false, false, pie_utils::EmptyPiePrice>;
template<typename R, R radius, typename P> using ApplePie = Pie<R, radius, true, true, P>;

#endif
